package de.thm.mni.bartkowski.genex.main;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ebi.kraken.interfaces.common.Sequence;
import uk.ac.ebi.kraken.interfaces.uniprot.DatabaseCrossReference;
import uk.ac.ebi.kraken.interfaces.uniprot.Gene;
import uk.ac.ebi.kraken.interfaces.uniprot.Keyword;
import uk.ac.ebi.kraken.interfaces.uniprot.NcbiTaxon;
import uk.ac.ebi.kraken.interfaces.uniprot.UniProtEntry;
import uk.ac.ebi.kraken.interfaces.uniprot.UniProtId;
import uk.ac.ebi.kraken.interfaces.uniprot.comments.Comment;
import uk.ac.ebi.kraken.interfaces.uniprot.features.Feature;
import uk.ac.ebi.kraken.interfaces.uniprot.features.FeatureLocation;
import uk.ac.ebi.uniprot.dataservice.client.Client;
import uk.ac.ebi.uniprot.dataservice.client.QueryResult;
import uk.ac.ebi.uniprot.dataservice.client.ServiceFactory;
import uk.ac.ebi.uniprot.dataservice.client.exception.ServiceException;
import uk.ac.ebi.uniprot.dataservice.client.uniprot.UniProtData;
import uk.ac.ebi.uniprot.dataservice.client.uniprot.UniProtData.ComponentType;
import uk.ac.ebi.uniprot.dataservice.client.uniprot.UniProtQueryBuilder;
import uk.ac.ebi.uniprot.dataservice.client.uniprot.UniProtService;
import uk.ac.ebi.uniprot.dataservice.client.uniprot.UniProtServiceInfo;
import uk.ac.ebi.uniprot.dataservice.query.Query;
import uk.ac.ebi.uniprot.dataservice.query.QueryFactory;

public class Main
{
	private final static Logger logger = LoggerFactory.getLogger(Main.class);
	
	public static void main(String... args)
	{
		//command line args
		int minimum = 800, maximum = 4000; double tolerance = 0.1;
		List<Double> searchMasses = Arrays.asList(849.94, 918.10, 1477.75, 1933.96, 2378.61, 2513.06, 2515.17, 2618.22, 2622.23, 3096.52);
		
		if (args.length >= 2)
		{
			minimum = Integer.valueOf(args[0]);
			maximum = Integer.valueOf(args[1]);
		}
		if (args.length >= 3)
		{
			tolerance = Double.valueOf(args[2]);
		}
		if (args.length >= 4)
		{
			Optional<Double> dOpt = isDouble(args[3]);
			if (dOpt.isPresent())
			{
				searchMasses = new ArrayList<>();
				searchMasses.add(dOpt.get());
				for (int i = 4; i < args.length; i++)
				{
					dOpt = isDouble(args[i]);
					if (dOpt.isPresent())
						searchMasses.add(dOpt.get());
				}
			}
			else
			{
				System.out.println("Assuming file path given...");
				String fpaths = args[3];
				Path fpath = Paths.get(fpaths);
				try
				{
					List<String> valid_lines = Files.readAllLines(fpath).stream().filter(s -> isDouble(s).isPresent()).collect(Collectors.toList());
					if (!valid_lines.isEmpty())
					{
						searchMasses = valid_lines.stream().map(s -> isDouble(s).get()).collect(Collectors.toList());
					}
					else
					{
						System.err.println("Sorry, but your file didn't contain any valid numbers!");
					}
				} catch (IOException e1)
				{
					e1.printStackTrace();
				}
			}
		}
		
		Map<String, UniProtEntry> masses = gatherMassEntriesHuman(minimum, maximum);
		if (masses.size() > 0)
		{
			System.out.println("--------------\nData collection successful! We have "+masses.size()+" results.\n--------------");
			//		Map<Double, List<UniProtEntry>> findings = new HashMap<>();
			System.out.println("Finding matches between masses of "+minimum+" and "+maximum+" for search masses " + searchMasses.stream().map(d -> d.toString()).collect(Collectors.joining(", ")));
			System.out.println("Applying tolerance of " + tolerance);
			for (String id : masses.keySet())
			{
				UniProtEntry upe = masses.get(id);
				Sequence seq = upe.getSequence();
				int mw = seq.getMolecularWeight();
				List<Double> matches = getMatches(mw, searchMasses, tolerance);
				if (!matches.isEmpty())
				{
					String msg = matches.stream().map(d -> d.toString()).collect(Collectors.joining(", "));
					System.out.println(id + " (" + mw + ") matches masses "+msg);
				}
				else
					System.out.println("No matches found for " + id); //+ ", mw="+mw);
			}
		}
		else
			System.err.println("No results found!");
	}
	
	private static Optional<Double> isDouble(String str)
	{
		try
		{
			return Optional.of(Double.valueOf(str));
		}
		catch (NumberFormatException e)
		{
			return Optional.empty();
		}
	}
	
	public static List<Double> getMatches(int mw, List<Double> searchMasses, double tolerance)
	{
		List<Double> matches = new ArrayList<>();
		for (Double d : searchMasses)
		{
			double dif = Math.abs(d-mw);
			double perc = dif / d;
			if (perc <= tolerance)
				matches.add(d);
		}
		return matches;
	}
	
	public static Map<String, UniProtEntry> gatherMassEntriesHuman(int minimumMass, int maximumMass)
	{
		if (minimumMass > 0 && maximumMass > 0)
		{
			if (maximumMass > minimumMass)
			{
				ServiceFactory serviceFactoryInstance = Client.getServiceFactoryInstance();
		        UniProtService uniProtService = serviceFactoryInstance.getUniProtQueryService();
				try
				{
					uniProtService.start();
					QueryFactory upf = UniProtQueryBuilder.factory();
					Query qhum = UniProtQueryBuilder.organismName("Homo sapiens (Human)");
					Query qhum_rew = UniProtQueryBuilder.reviewed(qhum, true);
					Query qmass = upf.createRangeQuery("mass", minimumMass+"", true, maximumMass+"", true);
					Query qall = UniProtQueryBuilder.and(qhum_rew, qmass);
					//accessMultiFullUniProtEntry(uniProtService, qall);
					//accessResults(uniProtService, qall);
					Map<String, UniProtEntry> masses = accessMasses(uniProtService, qall, false);
					UniProtServiceInfo info = uniProtService.getServiceInfo();
		            System.out.println(info.getReleaseNumber() +"\t" + info.getNumberOfTrEmblEntry());
		            return masses;
				} catch (Exception e) {
			        logger.error("drive examples", e);
			    } finally {
			        // always remember to stop the service
			        uniProtService.stop();
			        //System.out.println("service now stopped.");
			    }
				return null;
			}
			else
				throw new IllegalStateException("maximum mass must be > minimum mass!");
		}
		else
			throw new IllegalStateException("Masses must be > 0!");
	}
	
	public static void accessMultiFullUniProtEntry(UniProtService uniProtService, Query query) throws ServiceException {
        QueryResult<UniProtEntry> entries = uniProtService.getEntries(query);
        printExampleHeader("full entry");
        int count = 0;
        while (entries.hasNext()) {
            count++;
            UniProtEntry entry = entries.next();
            System.out.println(entry.getUniProtId().getValue());
        }
        System.out.println("retrieved entries = " + count);

    }
	

    public static void printExampleHeader(String headerTitle) {
        printExampleHeader("", headerTitle);
    }

    public static void printExampleHeader(String prefix, String headerTitle) {
        System.out.printf("%s=========== %s ==========%n", prefix, headerTitle);
    }
    
    public static Map<String, UniProtEntry> accessMasses(UniProtService uniProtService, Query query, boolean debug) throws ServiceException
    {
    	QueryResult<UniProtEntry> results = uniProtService.getEntries(query);
    	if (debug) System.out.println("number of hits = " + results.getNumberOfHits());
    	Map<String, UniProtEntry> map = new HashMap<>();
    	while (results.hasNext())
    	{
    		UniProtEntry upe = results.next();
    		UniProtId id = upe.getUniProtId();
    		map.put(id.getValue(), upe);
    		//PrimaryUniProtAccession acc = upe.getAccession();
    		Sequence seq = upe.getSequence();
    		int mw = seq.getMolecularWeight();
    		if (debug) System.out.printf("id = %s, mw = %d\n", id.getValue(), mw);
    	}
    	return map;
    }
    
//    public static void accessMasses(UniProtService uniProtService, Query query) throws ServiceException
//    {
//    	QueryResult<UniProtData> results = uniProtService.getResults(query);
//    	System.out.println("number of hits = " + results.getNumberOfHits());
//    	while (results.hasNext())
//    	{
//    		UniProtData upd = results.next();
//    		PrimaryUniProtAccession acc = upd.getAccession();
//    		UniProtEntry upe = uniProtService.getEntry(acc.getValue());
//    		Sequence seq = upe.getSequence();
//    		UniProtId id = upe.getUniProtId();
//    		int mw = seq.getMolecularWeight();
//    		System.out.printf("acc = %s, id = %s, mw = %d\n", acc.getValue(), id.getValue(), mw);
//    	}
//    }
    
    public static void accessResults(UniProtService uniProtService, Query query) throws ServiceException {
        printExampleHeader("accessResults (choose which fields you want to see)");
        QueryResult<UniProtData> results = uniProtService
                .getResults(query, ComponentType.COMMENTS, ComponentType.FEATURES, ComponentType.GENES);
        System.out.println("   number of hits = " + results.getNumberOfHits());
        while (results.hasNext()) {
            UniProtData data = results.next();
            System.out.println("   Accession: " + data.getAccession().getValue());
            System.out.println("   UniProtId: " + data.getUniProtId().getValue());
            for (ComponentType type : ComponentType.values()) {
                if (data.hasComponent(type)) {
                    switch (type) {
                        case COMMENTS:
                            printExampleHeader("   ", " comments");
                            List<Comment> comments = data.getComponent(ComponentType.COMMENTS);
                            for (Comment comment : comments) {
                                System.out.println("   " + "CommentType = " + comment.getCommentType()
                                        .toDisplayName() + ", " + "CommentStatus = " + comment.getCommentStatus().getValue());
                            }
                            break;
                        case FEATURES:
                            printExampleHeader("   ", "features");
                            List<Feature> features = data.getComponent(type);
                            for (Feature feature : features) {
                                StringBuilder featureSB = new StringBuilder();
                                FeatureLocation featureLocation = feature.getFeatureLocation();
                                featureSB.append(feature.getType().getName())
                                        .append(", ")
                                        .append(featureLocation.getStartModifier().toString())
                                        .append(featureLocation.getStart())
                                        .append(", ")
                                        .append(featureLocation.getEnd())
                                        .append(featureLocation.getEndModifier());

                                System.out.println("     " + featureSB.toString());
                            }
                            break;
                        case GENES:
                            printExampleHeader("   ", "genes");
                            List<Gene> genes = data.getComponent(type);
                            for (Gene gene : genes) {
                                System.out.println("     " + gene.getGeneName());
                            }
                            break;
                        case XREFS:
                            printExampleHeader("   ", "xrefs");
                            List<DatabaseCrossReference> xrefs = data.getComponent(type);
                            for (DatabaseCrossReference xref : xrefs) {
                                System.out.println("     " + xref.toString());
                            }
                            break;
                        case ECNUMBER:
                            printExampleHeader("   ", "ec numbers");
                            List<String> ecs = data.getComponent(type);
                            for (String ec : ecs) {
                                System.out.println("     " + ec);
                            }
                            break;
                        case PROTEIN_NAMES:
                            printExampleHeader("   ", "protein name");
                            List<String> proteins = data.getComponent(type);
                            for (String protein : proteins) {
                                System.out.println("     " + protein);
                            }

                            break;
                        case KEYWORDS:
                            printExampleHeader("   ", "keywords");
                            List<Keyword> kws = data.getComponent(type);
                            for (Keyword kw : kws) {
                                System.out.println("     " + kw.getValue());
                            }
                            break;
                        case TAXONOMY:
                            printExampleHeader("   ", "taxonomy");
                            List<NcbiTaxon> taxs = data.getComponent(type);
                            for (NcbiTaxon tax : taxs) {
                                System.out.println("     " + tax.getValue());
                            }
                            break;
                    }
                }
            }
        }

    }	
}	
